﻿This repository presents a comprehensive LC-MS/MS workflow for processing raw data acquired on an Agilent 6550 iFunnel Q-TOF LC/MS/MS instrument. The workflow utilizes various Bioconductor packages, including xmcs, CAMERA, and others, to perform efficient data processing and prepare the data for downstream analysis. The following steps outline the workflow:

# LC-MS/MS Workflow for Processing Raw Files

1. Input dataset collection and metadata file:Prepare the input dataset collection and metadata file required for the analysis.
2. Optimize the parameters for centwave, a peak detection algorithm used in LC-MS/MS data processing.
3. Create an 'MSnExp' object that combines the mzdata and metadata for further analysis. This object serves as the foundation for further analysis and integration of the data.
4. Apply the `msPurity::purityA` function for purity assessment.
5. Use xcms findChromPeaks to detect chromatographic peaks and generate an xcmsSet object.
6. Refine the chromatograms obtained from the previous step.
7. Group the chromatographic peaks using `xcms::groupChromPeaks` and generate a grouped xcmsSet object.
8. Quantify the abundances of the detected features and generate an assay matrix.
9. xcms::fillChromPeaks (fillPeaks): Exercise caution when using `xcms::fillChromPeaks` (fillPeaks) as it has the potential to fill in missing values, such as those in the Internal Standard in the solvent blanks. Assess the impact and suitability of using this function in your specific analysis. 
10. Assign MS/MS spectra to the grouped features using `msPurity::frag4feature`.
11. msPurity:filterFragSpectra: Flag and filter features based on signal-to-noise ratio, relative abundance, intensity threshold and precursor ion purity of the precursor.
12. Average the MS/MS spectra for each feature using `msPurity::averageFragSpectra`.
13. CAMERA annotation of the detected features
14. Create a database of MS/MS spectra using `msPurity::createDatabase`.
15. Connect to the sqlite library for further analysis.
16. Perform spectral matching using msPurity::spectralMatching.
17. Remove features with low intensities or those absent from samples.
18. Normalize the data by the internal standard (IS).
19. Select maximum abundant feature per pc-group (pseudocompound)
20. Combine matched results with unknowns
21. Matching MS2 spectra against massbank using Spectra package
22. Perform MS2 spectra matching for all files using the `MetaboAnnotation` package.
23. Subset the data to include only matched spectra.
24. Extract MS2 spectra per collision energy for parent ions


# Hardware Requirements

To successfully run the code in this repository, it is recommended to have a computer with sufficient hardware specifications. The code relies on in-memory operations, so a system with at least 16GB of RAM is recommended. The development and testing of this code were performed on a workstation equipped with an AMD Ryzen 9 7950X 4.5GHz Processor.

Please note that during the tutorial, we have limited the dataset to 16 .mzML files for faster processing. However, when applying this workflow to larger projects, certain processes may require significant computation time. To optimize the performance, consider the following suggestions:

`Parallel Processing`: Certain steps within the workflow can benefit from parallel execution. Utilizing parallel computing techniques can help speed up the overall processing time.

`Euler Cluster`: For computationally intensive tasks, it is advisable to run them on the Euler cluster. The cluster provides a high-performance computing environment that can efficiently handle resource-intensive computations.


# Files

This repository contains the following code files:

1. `lcmsms_preprocessing_20230512.Rmd`: This script serves as the main workflow and should be sourced to reproduce the LC-MS/MS preprocessing steps.
2. `lcmsms_IPO_20230512.R`: This script can be sourced to perform the centwave parameter optimization for the LC-MS/MS data.
3. `workspace_20230519.RData` R workspace containing the results from the analysis. You might need to generate again some objects (e.g. msdata, xcmsObj, etc) as the path for the raw files will be broken.

# SQLite MS2 Libraries

To facilitate the comparison of our MS2 spectra with publicly available libraries, it is crucial to download the following SQLite libraries:

1. `20210423_mona.sqlite`
   This library SQLite database contains data from MoNA, including Massbank, HMDB, LipidBlast, and GNPS. You can obtain it from the following source:
   [MoNA SQLite Database](https://github.com/computational-metabolomics/msp2db/releases/tag/v0.0.14-mona-23042021)

2. `MassbankSql-2021-03.db`
   An SQLite database version of the MassBank data, specifically formatted for MsBackendMassbankSql, is available for download here:
   [MassBank SQLite Database](https://github.com/jorainer/SpectraTutorials/releases/tag/2021.03)


# Results

To save time and avoid the need for lengthy computations to regenerate certain results within the workflow, I have provided a pre-generated workspace file named `workspace_20230519.RData`. This workspace file loads all the necessary objects generated by the source script. By loading this workspace, you can directly access the pre-computed results and proceed with downstream analyses without the need to re-run the entire workflow.
