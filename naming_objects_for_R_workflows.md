# Tips for writing objects in R and naming lc-ms and gc-ms files

In the lc-ms neg and pos and gc-ms polar and non-polar phases, KEEP the same name, except changing the type of the phase. This will save tons of time during the file reading in R. TONS OF TIME!!!

**DO NOT** start names (objects, samples, ANY NAME, etc) with numbers. R will automatically allocate an X at the start of the name

**DO NOT** use spaces for ANYTHING, including files, samples, treatments, R objects, etc, etc. Instead use underscore (_), not hyphen (-).
Avoid all special characters, except underscore
Avoid using CAPITALIZED letters in the treatment levels.
Use date format ISO 8601: YYYYMMDD.

Example: 
control_109_pos_date
control_109_neg_date
control_109_polar_date
control_109_nonpolar_date

General recommendations (not only for the R environment)

- Files should be named consistently.
- File names should be short but descriptive (<25 characters)
- Avoid special characters or spaces in a file name.
- Use capitals and underscores instead of periods or spaces or slashes.
- Use date format ISO 8601: YYYYMMDD.
